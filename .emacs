(add-to-list 'load-path (expand-file-name "~/.elisp/"))
(add-to-list 'load-path (expand-file-name "~/.elisp/remember"))
;;set display size and position
(setq initial-frame-alist '((top . 1) (left . 1) (width . 140) (height . 35)))

;; To get bitstream vera-sans as the default font
;; (require 'carbon-font)
;;  (create-fontset-from-fontset-spec
;;     "-apple-bitstream vera sans mono-medium-r-normal--16-*-*-*-*-*-fontset-mac,
;;      ascii:-apple-bitstream vera sans mono-medium-r-normal--16-*-*-*-m-*-mac-roman,
;;      latin-iso8859-1:-apple-bitstream vera sans mono-medium-r-normal--16-*-*-*-m-*-mac-roman,
;;      mule-unicode-0100-24ff:-apple-bitstream vera sans mono-medium-r-normal--16-*-*-*-m-*-mac-roman")
;;  (set-frame-font "-apple-bitstream vera sans mono-medium-r-normal--16-*-*-*-*-*-fontset-mac" 'keep)

;;personal niceties
(setq tramp-default-method "ssh")
(winner-mode t)
(global-font-lock-mode t)
(ediff-toggle-multiframe)
(menu-bar-enable-clipboard)

;;twiki mode
(require 'erin)

;;org-mode
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
;;custom TODO keywords
(setq org-todo-keywords '((type "SOMEDAY/MAY BE" "TODO" "NEXT ACTION" "WAITING FOR" "|" "DONE")))
;;custom agenda commands
(setq org-agenda-custom-commands
           '(("n" todo "NEXT ACTION")
             ("w" todo "WAITING FOR")
             ("s" todo "SOMEDAY/MAY BE")
             ("p" tags-todo "+phone")
             ("o" . "office+Name tags searches") ; description for "h" prefix
             ("ol" tags-todo "+office+laptop")
             ("oo" tags-todo "+office-laptop")))
;;settings for remember-mode
(require 'remember)
(org-remember-insinuate)
(setq org-directory "~/organization")
(setq org-default-notes-file (concat org-directory "/collection.org"))
(define-key global-map "\C-cr" 'org-remember)

;;for mac, to get the usual behavior for M-w which doesn't show on mac
(global-set-key "\M-w" 'copy-region-as-kill)
;;full screen
(defun toggle-fullscreen ()
  (interactive)
  (set-frame-parameter 
   nil 
   'fullscreen 
   (if (frame-parameter nil 'fullscreen)
       nil
     'fullboth)))

(global-set-key [(meta return)] 'toggle-fullscreen)

;; Use cperl-mode instead of the default perl-mode
(add-to-list 'auto-mode-alist '("\\.\\([pP][Llm]\\|al\\)\\'" . cperl-mode))
(add-to-list 'interpreter-mode-alist '("perl" . cperl-mode))
(add-to-list 'interpreter-mode-alist '("perl5" . cperl-mode))
(add-to-list 'interpreter-mode-alist '("miniperl" . cperl-mode))

(autoload 'javascript-mode' "javascript" nil t)
(autoload 'php-mode' "php-mode" nil t)
(autoload 'css-mode' "css-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.js\\'" . javascript-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . css-mode))


;;Steve Yegge's suggestions

(global-set-key "\C-w" 'delete-backward-char)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)
;lose the UI
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))

(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)

;;RDman's scroll other window
;;procedures for scrolling 1 line at a time for the other window
(defun scroll-other-window-n-lines-ahead (&optional n)
  "Scroll ahead N lines (1 by default) in the other window"
  (interactive "P")
  (scroll-other-window (prefix-numeric-value n)))

(defun scroll-other-window-n-lines-behind (&optional n)
  "Scroll behind N lines (1 by default) in the other window"
  (interactive "P")
  (scroll-other-window (- (prefix-numeric-value n))))

(global-set-key "\C-\M-a" 'scroll-other-window-n-lines-behind)
(global-set-key "\C-\M-z" 'scroll-other-window-n-lines-ahead)

;;WGE refers to Writing GNU Emacs Extensions
;traversing window functions (WGE Chapter 2)
(defun other-window-backward (&optional n)
  "Move the focus to nth previous window."
  (interactive "P")
  (other-window (- (prefix-numeric-value n))))

(global-set-key "\C-xn" 'other-window)
(global-set-key "\C-xp" 'other-window-backward)

;scrolling functions for scrolling one line at a time (WGE Chapter 2)
(defalias 'scroll-ahead 'scroll-up)
(defalias 'scroll-behind 'scroll-down)

(defun scroll-n-lines-ahead (&optional n)
  "scroll ahead n lines"
(interactive "P")
(scroll-ahead (prefix-numeric-value n)))

(defun scroll-n-lines-behind (&optional n)
  "scroll ahead n lines"
(interactive "P")
(scroll-behind (prefix-numeric-value n)))

(defun scroll-n-lines-left (&optional n)
  "scroll left n lines"
(interactive "P")
(scroll-left (prefix-numeric-value n)))

(defun scroll-n-lines-right (&optional n)
  "scroll left n lines"
(interactive "P")
(scroll-right (prefix-numeric-value n)))

(global-set-key "\C-z" 'scroll-n-lines-behind)
(global-set-key "\C-q" 'scroll-n-lines-ahead)
(global-set-key "\C-\M-a" 'scroll-n-lines-left)
(global-set-key "\C-\M-s" 'scroll-n-lines-right)
(global-set-key "\C-c\C-q" 'quoted-insert)

;defining desired behaviour when opening a symlink file(WGE Chapter 2)
(add-hook 'find-file-hooks 
	    (lambda ()
	          (if (file-symlink-p buffer-file-name)
		      (progn 
			  (setq buffer-read-only t)
			    (message "File is a symlink")))))

(defun visit-target-instead ()
  "Replace this buffer with a buffer visiting the link target"
  (interactive)
  (if buffer-file-name
      (let ((target (file-symlink-p buffer-file-name)))
	(if target
	        (find-alternate-file target)
	    (error "Not visiting a symlink")))
    (error "Not visiting a file")))

(defun clobber-symlink ()
  "Replace symlink with a copy of the file"
  (interactive)
  (if buffer-file-name
      (let ((target (file-symlink-p buffer-file-name)))
	(if target 
	        (if (yes-or-no-p (format "Replace %s with %s?" buffer-file-name target))
		    (progn
		        (delete-file buffer-file-name)
			  (write-file buffer-file-name)))
	    (error "Not visiting a symlink")))
    (error "Not visiting a file")))

;undoing scrolling (WGE Chapter 3)
(defvar unscroll-point nil
  "Cursor position for next call to 'unscroll'.")
(defvar unscroll-window-start nil
  "Window start for next call to 'unscroll'.")

(defadvice scroll-up (before remember-for-unscroll
			          activate compile)
  "Remember where we started from, for 'unscroll'."
(if (not (eq last-command 'scroll-up))
    (progn 
      (setq unscroll-point (point))
      (setq unscroll-window-start (window-start)))))
(defun unscroll ()
  "Jump to the position specified by 'unscroll-to'."
  (interactive)
  (if (and (not unscroll-point) (not unscroll-window-start))
      (error "cannot unscroll yet"))
  (progn
    (goto-char unscroll-point)
    (set-window-start nil unscroll-window-start)))

;;For reading word documents
(when (locate-library "no-word")
  (require 'no-word)
  (add-to-list 'auto-mode-alist '("\\.doc\\'" . no-word)))

;;nxml mode
(load "~/.elisp/nxml-mode/rng-auto.el")
(setq auto-mode-alist
      (cons '("\\.\\(xml\\|xsl\\|rng\\|xhtml\\)\\'" . nxml-mode)
            auto-mode-alist))

;;RDman's workflow mode
(load "~/.elisp/pw-utils/pw-utils.el")
;;artist mode
(autoload 'artist-mode "artist" "Enter artist-mode" t)
;;replace tabs with spaces
(setq-default indent-tabs-mode nil)


(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(fill-column 125)
 '(org-agenda-files (quote ("~/org_files/office.org" "~/org_files/gtd.org" "~/organization/delhi_barista_meeting.org"))))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(default ((t (:stipple nil :background "black" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 200 :width normal :family "inconsolata")))))

